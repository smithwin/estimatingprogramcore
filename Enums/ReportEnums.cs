﻿namespace EstimatingProgramCore.Enums
{
    public enum ReportEnums
    {
        QuantitiesAllowablesTotal = 11,
        QuantitiesAllowablesProgressive = 12
    }
}
